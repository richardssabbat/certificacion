/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Conectar;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author cristian
 */
public class Conectar {

    public static final Connection getConnection(){
        
        String driver="com.mysql.jdbc.Driver";
        String usuario="root";
        String pass="";
        String url= "jdbc:mysql://localhost:3306/panilla";
        
        Connection conn=null;
        try{
            
            Class.forName(driver);
            conn= DriverManager.getConnection(url,usuario,pass);
            System.out.println("conectado");
            
        }catch(SQLException ex){
            System.out.println("error al tratar de conectarse con la base de Datos: " + ex);
            ex.printStackTrace();
            
        }catch(ClassNotFoundException ex){ 
            System.out.println("No se puede conectar a la base de datos "+ex);
            ex.printStackTrace();
        }
        return conn;
    }      

    public void close() {
        System.out.println("cerrando sesion");
    }
}
