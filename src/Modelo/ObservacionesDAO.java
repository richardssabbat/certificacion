/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import Conectar.Conectar;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import vista.Vista;

/**
 *
 * @author cristian
 */
public class ObservacionesDAO {
    Conectar conn = new Conectar();
    String mensaje = "";
    
    public String agregarObservaciones (Vista vista) {
    
        PreparedStatement ps = null;
        
        String sql = "INSERT INTO observaciones (idobservaciones, calleLineo,regeneracion,"
                + "descripcion,idInventario) VALUES (NULL,?,?,?,?)";
        
        try {
            ps = conn.getConnection().prepareStatement(sql);
            ps.setInt(1, Integer.BYTES);
            ps.setString(2, vista.txtarea.getText());
            ps.setString(2, vista.txtregeneracion.getText());
            ps.setInt(5, Integer.parseInt(vista.txtidinventario.getText()));
            ps.execute();
            
            mensaje = "El registro se agrego correctamente";
            
        } catch (Exception e) {
            mensaje = "No se pudo agregar el registro";
            e.printStackTrace();
        }finally{
            if(conn!=null){
                try {
                    ps.close();
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(Observaciones.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } 
        return mensaje;
    }
    public String modificarObservaciones (Vista vista) {
    
        PreparedStatement ps = null;
        
        String sql = "UPDATE observaciones SET idobservaciones = ?, calleLineo = ?,regeneracion = ?,"
                + "descripcion = ?,idInventario = ?";
        
        try {
            ps = conn.getConnection().prepareStatement(sql);
            ps.setInt(1, Integer.parseInt(vista.txtidobservaciones.getText()));
            ps.setString(2, vista.txtarea.getText());
            ps.setString(2, vista.txtregeneracion.getText());
            ps.setInt(5, Integer.parseInt(vista.txtidinventario.getText()));
            ps.execute();
            
            mensaje = "El registro se actualizo correctamente";
        } catch (Exception e) {
            mensaje = "No se pudo actualizar el registro";
            e.printStackTrace();
        }finally{
            if(conn!=null){
                try {
                    ps.close();
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(Observaciones.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return mensaje;
    }
    public String eliminarObservaciones (Vista vista){
    
        PreparedStatement ps = null;
        
        String sql = "DELETE FROM observaciones WHERE idobservaciones = ?";
        
        try {
            ps = conn.getConnection().prepareStatement(sql);
            ps.setInt(1, Integer.parseInt(vista.txtidobservaciones.getText()));
            ps.execute();
            
            mensaje = "El registro se elimino correctamente";
        } catch (Exception e) {
            mensaje = "No se pudo eliminar el registro";
            e.printStackTrace();
        }
        return mensaje;  
    }
}
