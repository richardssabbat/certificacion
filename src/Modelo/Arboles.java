/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.math.BigDecimal;

/**
 *
 * @author cristian
 */
public class Arboles {
    private int idarbol;
    private String escpecie;
    private float calidad;
    private String podable;
    private String noPodable;
    private int idparcela;
    private double hDosMetros;
    private double hDosCentimetros;
    private int hPoda;
    private int hVDos;
    private double htotal;
    private String marcacion;
    private String defectoCorregible;
    private String defectoNoCorregible;
    private int altuDefecto;
    private String tipoDefecto;
    private int arbolesPorParcela;
    private String seleccion;

    public Arboles() {
    }

    public String getDefectoCorregible() {
        return defectoCorregible;
    }

    public void setDefectoCorregible(String defectoCorregible) {
        this.defectoCorregible = defectoCorregible;
    }

    public String getDefectoNoCorregible() {
        return defectoNoCorregible;
    }

    public void setDefectoNoCorregible(String defectoNoCorregible) {
        this.defectoNoCorregible = defectoNoCorregible;
    }

    public String getSeleccion() {
        return seleccion;
    }

    public void setSeleccion(String seleccion) {
        this.seleccion = seleccion;
    }

    public int getIdarbol() {
        return idarbol;
    }

    public void setIdarbol(int idarbol) {
        this.idarbol = idarbol;
    }

    public String getEscpecie() {
        return escpecie;
    }

    public void setEscpecie(String escpecie) {
        this.escpecie = escpecie;
    }

    public float getCalidad() {
        return calidad;
    }

    public void setCalidad(float calidad) {
        this.calidad = calidad;
    }

    public String getPodable() {
        return podable;
    }

    public void setPodable(String podable) {
        this.podable = podable;
    }

    public String getNoPodable() {
        return noPodable;
    }

    public void setNoPodable(String noPodable) {
        this.noPodable = noPodable;
    }

    public int getIdparcela() {
        return idparcela;
    }

    public void setIdparcela(int idparcela) {
        this.idparcela = idparcela;
    }

    public double gethDosMetros() {
        return hDosMetros;
    }

    public void sethDosMetros(double hDosMetros) {
        this.hDosMetros = hDosMetros;
    }

    public double gethDosCentimetros() {
        return hDosCentimetros;
    }

    public void sethDosCentimetros(double hDosCentimetros) {
        this.hDosCentimetros = hDosCentimetros;
    }

    public int gethPoda() {
        return hPoda;
    }

    public void sethPoda(int hPoda) {
        this.hPoda = hPoda;
    }

    public int gethVDos() {
        return hVDos;
    }

    public void sethVDos(int hVDos) {
        this.hVDos = hVDos;
    }

    public double getHtotal() {
        return htotal;
    }

    public void setHtotal(double htotal) {
        this.htotal = htotal;
    }

    public String getMarcacion() {
        return marcacion;
    }

    public void setMarcacion(String marcacion) {
        this.marcacion = marcacion;
    }

    public int getAltuDefecto() {
        return altuDefecto;
    }

    public void setAltuDefecto(int altuDefecto) {
        this.altuDefecto = altuDefecto;
    }

    public String getTipoDefecto() {
        return tipoDefecto;
    }

    public void setTipoDefecto(String tipoDefecto) {
        this.tipoDefecto = tipoDefecto;
    }

    public int getArbolesPorParcela() {
        return arbolesPorParcela;
    }

    public void setArbolesPorParcela(int arbolesPorParcela) {
        this.arbolesPorParcela = arbolesPorParcela;
    }
    
}
