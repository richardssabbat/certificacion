/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import Conectar.Conectar;
import java.math.BigDecimal;
import java.sql.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JList;
import vista.Vista;

/**
 *
 * @author cristian
 */
public class ArbolesDAO {
    
    Conectar conn = new Conectar();
    
    String mensaje="";
    
    public String agregarArboles(Vista vista){
        
        PreparedStatement ps = null;
       
        String sql= "INSERT INTO arboles (especie,calidad,podable,noPodable,idparcela,hDosMetros,hDosCentimetros,"
                + "hPoda,hVDos,htotal,marcacion,seleccion,defectoCorregible,defectoNoCorregible,alturaDefecto,tipoDefecto,"
                + "numeroArbolesParcela) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        
        try {
            ps = conn.getConnection().prepareStatement(sql);
            ps.setString (1, vista.txtespecie.getText());
            ps.setDouble(2, Double.parseDouble(vista.txtcalidad.getText()));
            ps.setString(3, vista.Radiosi.getText().toString());
            ps.setString(4, vista.Radiono.getText().toString());
            ps.setInt(5, Integer.BYTES);
            ps.setDouble(6, Double.parseDouble(vista.txtdosmm.getText()));
            ps.setDouble(7, Double.parseDouble(vista.txthdoscm.getText()));
            ps.setInt (8, Integer.parseInt(vista.txthpoda.getText()));
            ps.setInt(9, Integer.parseInt(vista.txthvdos.getText()));
            ps.setDouble(10, Double.parseDouble(vista.txthtotal.getText()));
            ps.setString(11, vista.txtmarcacion.getText());
            ps.setString(12, vista.checkseleccionado.getText());
            ps.setString(13, vista.checkdefectocorregible.getText());
            ps.setString(14, vista.checkdefectonocorregible.getText());
            ps.setInt(15, Integer.parseInt(vista.txtalturadefecto.getText()));
            ps.setString(16, vista.txttipodefecto.getText());
            ps.setInt(17, Integer.parseInt(vista.txtnumarbolesPodados.getText()));
            ps.executeUpdate();
            
            mensaje="los datos se guardaron correctamente";
            System.out.println(mensaje);
     
        } catch (SQLException e) {
            mensaje = "no se pudo guardar los datos";
            System.out.println(mensaje);
            e.printStackTrace();
        }finally{
            if(conn!=null){
                try {
                    ps.close();
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(Arboles.class.getName()).log(Level.SEVERE, null, ex);
            }
        
        }
    }return mensaje;
}
        public String actualizarArbol(Vista vista){
        
        PreparedStatement ps = null;
       
        String sql= "UPDATE arboles SET especie = ?,calidad = ?,marcacion = ?,podable = ?,noPodable = ?,idparcela = ?,hDosMetros = ?,hDosCentimetros  = ?,"
                + "hPoda = ?,hVDos = ?,htotal = ?,seleccion = ?,defectoCorregible = ?,defectoNoCorregible = ?,alturaDefecto = ?,tipoDefecto = ?,"
                + "numeroArbolesParcela = ? WHERE idarbol = ?";
        
        try {
            ps = conn.getConnection().prepareStatement(sql);
            ps.setInt   (1, Integer.parseInt(vista.txtidparcela.getText()));
            ps.setString (2, vista.txtespecie.getText());
            ps.setFloat (3,Float.parseFloat(vista.txtcalidad.getText()));
            ps.setString(4, vista.Radiosi.getText());
            ps.setString(5, vista.Radiono.getText());
            ps.setInt(6, Integer.parseInt(vista.txtidparcela.getText()));
            ps.setDouble(7, Double.parseDouble(vista.txtdosmm.getText()));
            ps.setDouble(8, Double.parseDouble(vista.txthdoscm.getText()));
            ps.setInt (9, Integer.parseInt(vista.txthpoda.getText()));
            ps.setInt(10, Integer.parseInt(vista.txthvdos.getText()));
            ps.setDouble(11, Double.parseDouble(vista.txthtotal.getText()));
            ps.setString(12, vista.txtmarcacion.getText());
            ps.setString(13, vista.checkseleccionado.getText());
            ps.setString(14, vista.checkdefectocorregible.getText());
            ps.setString(15, vista.checkdefectonocorregible.getText());
            ps.setInt(16, Integer.parseInt(vista.txtalturadefecto.getText()));
            ps.setString(17, vista.txttipodefecto.getText());
            ps.setInt(18, Integer.parseInt(vista.txtnumarbolesPodados.getText()));
            ps.execute();
            mensaje="los datos se actualizaron correctamente";
     
        } catch (SQLException e) {
            mensaje = "no se pudo actualizar los datos";
            e.printStackTrace();
        }finally{
            if(conn!=null){
                try {
                    ps.close();
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(Arboles.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }    
        return mensaje;
} 
    public String eliminarArbol( Vista vista){
        PreparedStatement ps = null;
       
        String sql= "DELETE FROM arboles WHERE idarbol = ?";
        
        try {
            ps = conn.getConnection().prepareStatement(sql);
            ps.setInt(1, Integer.parseInt(vista.txtidparcela.getText()));
            ps.execute();
            
            mensaje="los datos fueron removidos correctamente";
     
        } catch (SQLException e) {
            mensaje = "no se pudo borrar los datos";
            e.printStackTrace();
        }finally{
            if(conn!=null){
                try {
                    ps.close();
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(Arboles.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } 
        return mensaje;  
    }
    public ArrayList getArboles (){
    
        PreparedStatement ps=null;
        ResultSet rs=null;
        String sql = "Selec * from arboles where idparcela = ?";
        Conectar conn = new Conectar();
        Arboles ar = null;
            ArrayList<ArbolesDAO> lista= null;
            try {
                ps = conn.getConnection().prepareCall(sql);
                rs = ps.executeQuery();
                lista = new ArrayList<ArbolesDAO>();
                
                while (rs.next()){
                ar.getIdparcela();
                ar.getArbolesPorParcela();
                lista.add(this);    
                }       
        } catch (Exception e) {
        }
        return lista;
    }
}
