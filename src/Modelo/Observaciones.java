/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author cristian
 */
public class Observaciones {
    private int idObservaciones;
    private String calleLineo;
    private String regeneracion;
    private String descripcion;
    private int ideInventario;

    public Observaciones() {
    }

    public int getIdObservaciones() {
        return idObservaciones;
    }

    public void setIdObservaciones(int idObservaciones) {
        this.idObservaciones = idObservaciones;
    }

    public String getCalleLineo() {
        return calleLineo;
    }

    public void setCalleLineo(String calleLineo) {
        this.calleLineo = calleLineo;
    }

    public String getRegeneracion() {
        return regeneracion;
    }

    public void setRegeneracion(String regeneracion) {
        this.regeneracion = regeneracion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getIdeInventario() {
        return ideInventario;
    }

    public void setIdeInventario(int ideInventario) {
        this.ideInventario = ideInventario;
    }
    
    
}
