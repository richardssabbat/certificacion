/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.Date;

/**
 *
 * @author cristian
 */
public class Inventario {
    private int idInventario;
    private String empresaContratante;
    private String empresaPoda;
    private String empresaCertificadora;
    private String certificador;
    private String cargo;
    private Date fecha;
    private String tipoPoda;
    private int numerosArbolesPodados;
    private double alturaDePoda;
    private int unidadMuestral;

    public int getUnidadMuestral() {
        return unidadMuestral;
    }

    public void setUnidadMuestral(int unidadMuestral) {
        this.unidadMuestral = unidadMuestral;
    }
    public Inventario() {
    }

    public int getIdInventario() {
        return idInventario;
    }

    public void setIdInventario(int idInventario) {
        this.idInventario = idInventario;
    }

    public String getEmpresaContratante() {
        return empresaContratante;
    }

    public void setEmpresaContratante(String empresaContratante) {
        this.empresaContratante = empresaContratante;
    }

    public String getEmpresaPoda() {
        return empresaPoda;
    }

    public void setEmpresaPoda(String empresaPoda) {
        this.empresaPoda = empresaPoda;
    }

    public String getEmpresaCertificadora() {
        return empresaCertificadora;
    }

    public void setEmpresaCertificadora(String empresaCertificadora) {
        this.empresaCertificadora = empresaCertificadora;
    }

    public String getCertificador() {
        return certificador;
    }

    public void setCertificador(String certificador) {
        this.certificador = certificador;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getTipoPoda() {
        return tipoPoda;
    }

    public void setTipoPoda(String tipoPoda) {
        this.tipoPoda = tipoPoda;
    }

    public int getNumerosArbolesPodados() {
        return numerosArbolesPodados;
    }

    public void setNumerosArbolesPodados(int numerosArbolesPodados) {
        this.numerosArbolesPodados = numerosArbolesPodados;
    }

    public double getAlturaDePoda() {
        return alturaDePoda;
    }

    public void setAlturaDePoda(double alturaDePoda) {
        this.alturaDePoda = alturaDePoda;
    }
    
}
