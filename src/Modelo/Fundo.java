/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author cristian
 */
public class Fundo {
    private int idFundo;
    private String nombreFundo;
    private int idInventario;

    public Fundo() {
    }

    public int getIdFundo() {
        return idFundo;
    }

    public void setIdFundo(int idFundo) {
        this.idFundo = idFundo;
    }

    public String getNombreFundo() {
        return nombreFundo;
    }

    public void setNombreFundo(String nombreFundo) {
        this.nombreFundo = nombreFundo;
    }

    public int getIdInventario() {
        return idInventario;
    }

    public void setIdInventario(int idInventario) {
        this.idInventario = idInventario;
    }
    
    
}
