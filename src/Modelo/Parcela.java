/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author cristian
 */
public class Parcela {
    private int idParcela;
    private String formaParcela;
    private double subParMetrosCuadrados;
    private int poligono;
    private int idInventario;
    private double latitud;
    private double longitud;

    public Parcela() {
    }

    public int getIdParcela() {
        return idParcela;
    }

    public void setIdParcela(int idParcela) {
        this.idParcela = idParcela;
    }

    public String getFormaParcela() {
        return formaParcela;
    }

    public void setFormaParcela(String formaParcela) {
        this.formaParcela = formaParcela;
    }

    public double getSubParMetrosCuadrados() {
        return subParMetrosCuadrados;
    }

    public void setSubParMetrosCuadrados(double subParMetrosCuadrados) {
        this.subParMetrosCuadrados = subParMetrosCuadrados;
    }

    public int getPoligono() {
        return poligono;
    }

    public void setPoligono(int poligono) {
        this.poligono = poligono;
    }

    public int getIdInventario() {
        return idInventario;
    }

    public void setIdInventario(int idInventario) {
        this.idInventario = idInventario;
    }

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }
    
}
