
package Modelo;

import Conectar.Conectar;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import vista.Vista;

 
public class FundoDAO {
    Conectar conn = new Conectar();
    String mensaje = "";
    public String agregarFundo (Vista vista){
        
        PreparedStatement ps = null;
        
        
        String sql = "INSERT INTO fundo (idfundo,nombreFundo,idinventario)"
                + "VALUES (NULL,?,?)";
         
        try {
            ps=conn.getConnection().prepareStatement(sql);
            ps.setString(1, vista.txtfundo.getText());
            ps.setInt(2, Integer.parseInt(vista.txtidinventario.getText()));
            ps.execute();
            
            mensaje="se agrego correctamente";
        } catch (Exception e) {
            mensaje="no se pudo agregar el registro";
            e.printStackTrace();
        }finally{
            if(conn!=null){
                try {
                    ps.close();
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(Fundo.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }return mensaje;
    }
    public String modificarFundo (Vista vista){
    
        PreparedStatement ps = null;
        
        String sql="UPDATE fundo SET idfundo = ?,nombreFundo = ?,idinventario = ? WHERE idfundo = ?";
        
          try {
            ps=conn.getConnection().prepareStatement(sql);
            //ps.setInt(1, ); (corregir)
            ps.setString(2, vista.txtfundo.getText());
            ps.setInt(3, Integer.parseInt(vista.txtidinventario.getText()));
            ps.execute();
            
            mensaje="se actualizo correctamente";
        } catch (Exception e) {
            mensaje="no se pudo actualizar el registro";
            e.printStackTrace();
        }finally{
            if(conn!=null){
                try {
                    ps.close();
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(Fundo.class.getName()).log(Level.SEVERE, null, ex);
                } ;
            }
        }
        return null;
    }
    public String eliminarFundo (Vista vista){
    
        PreparedStatement ps = null;
        
        String sql = "DELETE FROM fundo WHERE idfundo = ?";
        
        try {
            ps = conn.getConnection().prepareStatement(sql);
            ps.setInt(1, Integer.parseInt(vista.txtfundo.getText()));
            ps.execute();
        }catch (SQLException e) {
            mensaje = "no se pudo borrar los datos";
            e.printStackTrace();
        }finally{
            if(conn!=null){
                try {
                    ps.close();
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(Fundo.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } 
        return mensaje; 
    }
    public String listarfundo (Connection conn, int id){
        return null;   
    }
}
