/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import Conectar.Conectar;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTextField;
import vista.Vista;

/**
 *
 * @author cristian
 */
public class ParcelaDAO {
    
    Conectar conn = new Conectar();
    String mensaje = "";
    
    public String agregarParcela (Vista vista){
        
        PreparedStatement ps = null;
        
        String sql = "INSERT INTO parcela(idparcela,formaParcela,subParMetrosCuadrados,poligonos,idInventario"
                + "latitud,longitud) VALUES (NULL,?,?,?,?,?,?)";
        
        try {
            ps = conn.getConnection().prepareStatement(sql);
            ps.setInt(1, Integer.BYTES);
            ps.setString(2, vista.txtformaparcela.getText());
            ps.setDouble(3, Double.parseDouble(vista.txtsupparcela.getText()));
            ps.setInt(4, Integer.parseInt(vista.txtnpoligono.getText()));
            ps.setInt(5, Integer.parseInt(vista.txtidinventario.getText()));
            ps.setDouble(6, Double.parseDouble(vista.txtlatitud.getText()));
            ps.setDouble(7, Double.parseDouble(vista.txtlongitud.getText()));
            ps.execute();
            
            mensaje = "El registro se agrego correctamente";
        } catch (Exception e) {
            mensaje = "No se puede agregar el registro";
            e.printStackTrace();
        }finally{
            if(conn!=null){
                try {
                    ps.close();
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(Parcela.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return mensaje;  
    }
    public String modificarParcela (Vista vista){
    
        PreparedStatement ps = null;
        
        String sql = "UPDATE parcela SET idparcela = ?,formaParcela = ?,subParMetrosCuadrados = ?,"
                + "poligonos = ?,idInventario = ?,latitud = ?,longitud = ? WHERE idárcela = ?";
        
        try {
            ps = conn.getConnection().prepareStatement(sql);
            ps.setInt(1, Integer.BYTES);
            ps.setString(2, vista.txtformaparcela.getText());
            ps.setDouble(3, Double.parseDouble(vista.txtsupparcela.getText()));
            ps.setInt(4, Integer.parseInt(vista.txtnpoligono.getText()));
            ps.setInt(5, Integer.parseInt(vista.txtidinventario.getText()));
            ps.setDouble(6, Double.parseDouble(vista.txtlatitud.getText()));
            ps.setDouble(7, Double.parseDouble(vista.txtlongitud.getText()));
            ps.execute();
            
            mensaje = "El registro se actualizo correctamente";
        } catch (Exception e) {
            mensaje = "No se pudo actulizar el registro";
            e.printStackTrace();
        }finally{
            if(conn!=null){
                try {
                    ps.close();
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(Parcela.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return mensaje;
    }
    public String eliminarParcela (Vista vista){
    
        PreparedStatement ps = null;
        
        String sql = "DELETE FROM parcela WHERE idparcela = ?";
        
        try {
            ps = conn.getConnection().prepareStatement(sql);
            ps.setInt(1, Integer.parseInt(vista.txtidparcela.getText()));
            ps.execute();
            
            mensaje = "El registro se elimino correctamente";
        } catch (Exception e) {
            mensaje = "No se pudo el eliminar el registro";
            e.printStackTrace();;
        }finally{
            if(conn!=null){
                try {
                    ps.close();
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(Parcela.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return mensaje;
    }
}
