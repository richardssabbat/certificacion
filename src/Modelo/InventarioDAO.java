/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import Conectar.Conectar;
import static com.sun.org.apache.xalan.internal.lib.ExsltDatetime.date;
import com.toedter.calendar.IDateEditor;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JTextField;

import vista.Vista;

/**
 *
 * @author cristian
 */
public class InventarioDAO {
 
    Conectar conn = new Conectar();
    
    String mensaje = "";
    
    public String agregarInventario (Vista vista){
        
        PreparedStatement ps = null;
        
        String sql = "INSERT INTO inventario (empresaContratante,empresaPoda,empresaCertificadora,certificador,"
                + "cargo,fecha,tipoPoda,numeroArbolesPodados,latitud,longitud)"
                + "VALUES (?,?,?,?,?,?,?,?,?,?);";
        
        try {
            ps= conn.getConnection().prepareStatement(sql);
            ps.setString(1,vista.txtemserforcontratante.getText());
            ps.setString(2,vista.txtemseforpoda.getText());
            ps.setString(3,vista.txtemseforcertificador.getText());
            ps.setString(4,vista.txtjefedecuadrilla.getText());
            ps.setString(5,vista.txtcargo.getText());
            ps.setString(6, vista.txtfecha.getText());
            ps.setString(7, vista.txttipopoda.getText());
            ps.setInt(8,Integer.parseInt(vista.txtnumarbolesPodados.getText()));
            ps.setDouble(9, Double.parseDouble(vista.txtlatitud.getText()));
            ps.setDouble(10,Double.parseDouble(vista.txtlongitud.getText()));
            ps.execute();
            
            mensaje = "El registro se agrego correctamente";
            System.out.println(mensaje);
        } catch (Exception e) {
            
            mensaje = "No se pudo guardar el registro!"+e;
            System.out.println(mensaje);
            e.printStackTrace();
        }finally{
            if(conn!=null){
                try {
                    ps.close();
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(InventarioDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } 
        return mensaje;   
    }
    
    public String modificarInventario (Vista vista){
        
        PreparedStatement ps = null;
        
        String sql = "UPDATE inventario SET empresaContratante = ?,"
                + "empresaPoda = ?,empresaCertificadora = ?,certificador = ?,"
                + "cargo = ?,fecha = ?,tipoPoda = ?,numeroArbolesPodados = ?,"
                + "latitud = ?, longitud = ?"
                + "WHERE idInventario = ?";
        
        try {
            ps= conn.getConnection().prepareStatement(sql);
            
            ps.setString(1,vista.txtemserforcontratante.getText());
            ps.setString(2,vista.txtemseforpoda.getText());
            ps.setString(3,vista.txtemseforcertificador.getText());
            ps.setString(4,vista.txtjefedecuadrilla.getText());
            ps.setString(5,vista.txtcargo.getText());
            ps.setString(6, vista.txtfecha.getText());
            ps.setString(7, vista.txttipopoda.getText());
            ps.setInt(8,Integer.parseInt(vista.txtnumarbolesPodados.getText()));
            ps.setDouble(9, Double.parseDouble(vista.txtlatitud.getText()));
            ps.setDouble(10,Double.parseDouble(vista.txtlongitud.getText()));
            ps.setInt(11, Integer.parseInt(vista.txtidinventario.getText()));
            ps.execute();
           
            
            mensaje = "el inventario se actualizo correctamente";
            System.out.println(mensaje);
        } catch (Exception e) {
            mensaje = "no se puedo acttulizar el inventario";
            System.out.println(mensaje);
            e.printStackTrace();
        }finally{
            if(conn!=null){
                try {
                    ps.close();
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(Inventario.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } 
        return mensaje;
    }
    public String eliminarInventario ( Vista vista){
    
        PreparedStatement ps = null;
        
        String sql = "DELETE FROM inventario WHERE idinventario = ?";
        
        try {
            ps = conn.getConnection().prepareStatement(sql);
            ps.setInt(1, Integer.parseInt(vista.txtidinventario.getText()));
            ps.execute();
            
            mensaje = "El registro se elimino correctamente";
            System.out.println(mensaje);
        } catch (Exception e) {
            mensaje = "No se pudo borrar el registro";
            System.out.println(mensaje);
            e.printStackTrace();
        }finally{
            if(conn!=null){
                try {
                    ps.close();
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(Inventario.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } 
        return mensaje;
    }
}
